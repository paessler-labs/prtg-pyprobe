# Run the prtg-pyprobe on a Synology NAS    
## docker
- Install the [Docker Package](https://www.synology.com/en-global/dsm/packages/Docker) on your NAS.    
- In theory, it should be possible to add the [project container registry](https://gitlab.com/paessler-labs/prtg-pyprobe/container_registry) to the registries used by the docker package by selecting **Add** under **Registry**. Then you should be able to search for tags. (If you get it working, let me know. All I got was *Failed to query registry.*)
- If the theoretical step from before did not work, just ssh into your Synology NAS and do a docker pull for the image.
- The pulled image should now show up under images 
  ![Docker Image Synology](../img/synology_docker_image.png){: align=left }
- Now simply click **Launch** and adjust the **Container Name** and enable **Execute container using high privilege**
  ![Docker Image Synology](../img/synology_create_container.png){: align=left }
- Afterwards go to **Advanced Settings - Environment** and add a variable **PROBE_CONFIG** with contents as described [here](../index.md#format-of-the-config_dict-object).
  ![Docker Image Synology](../img/synology_create_container_advanced_settings.png){: align=left }
- Hit **Apply** and then **Next**
- In the Summary check your settings and enable **Run this container after the wizard is finished**
  ![Docker Image Synology](../img/synology_create_container_summary.png){: align=left }
- Now head to your PRTG Instance and approve the new probe.
## native
- We have not tested a native installation using the [Python3 package](https://www.synology.com/en-us/dsm/packages/py3k) but you should be able to use the probe as described [here](../index.md#native-installation).
