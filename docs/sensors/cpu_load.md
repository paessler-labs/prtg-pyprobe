---
**WARNING**  
Although this sensor can be added to every device on the probe, it will **ONLY** monitor the probe itself.  

---
#### Description
Monitors CPU load avg on the system the pyprobe is running on.

---
#### Configuration
This sensor needs no configuration.

---
![CPU Load](../img/sensor_cpu_load.png){: align=left }
