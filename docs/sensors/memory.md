---
**WARNING**  
Although this sensor can be added to every device on the probe, it will **ONLY** monitor the probe itself.  

---
#### Description
Sensor used to monitor the memory usage of the system on which the python probe is running.

---
#### Configuration
This sensor needs no configuration.

---
![Memory](../img/sensor_memory.png){: align=left }
