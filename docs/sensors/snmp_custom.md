---
#### Description
Monitors a numerical value returned by a specific OID using SNMP.

---
#### Configuration
**Custom OID**: Provide an OID which is returning a numeric value.  
**Custom Unit**:  Provide an unit to be shown with the value.  
**Value Type**:  Select 'Gauge' if you want to see absolute values (e.g. for temperature value) or 'Delta' for counter differences divided by time period (e.g. for bandwidth values).  
**Community String**:  Enter the community string.  
**SNMP Port**:  Enter the SNMP Port.  
**SNMP Version**: Choose your SNMP Version.

---
![SNMP Custom](../img/sensor_snmp_custom.png){: align=left }
