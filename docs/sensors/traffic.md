---
**WARNING**  
Although this sensor can be added to every device on the probe, it will **ONLY** monitor the probe itself.  

---
#### Description
Sensor used to monitor the temperature of the Python probe.

---
#### Configuration
**Interface Name**:  Provide the interface name to monitor.   

---
![Traffic](../img/sensor_traffic.png){: align=left }
