before_script:
    - pip3 install -r requirements-dev.txt

variables:
  PYTHON_DEBIAN_IMAGE: "python:3.8-buster"
  DEBIAN_IMAGE: "debian:buster"
  KANIKO_IMAGE: "gcr.io/kaniko-project/executor:debug-v1.0.0"

stages:
  - version
  - check
  - unit-test
  - make-docs
  - pip-build
  - pip-deploy
  - build
  - deploy

version:
  stage: version
  only:
    - release
  image: $PYTHON_DEBIAN_IMAGE
  before_script: []
  script:
    - PYPROBE_TAG=$(git describe)
    - PYPROBE_VERSION=$(echo $PYPROBE_TAG | cut -d "-" -f 1)
    - echo PYPROBE_VERSION=$PYPROBE_VERSION >> build.env
  artifacts:
    reports:
      dotenv: build.env

check_formatting:
  stage: check
  image: $PYTHON_DEBIAN_IMAGE
  script:
    - black -l 120 --check tests
    - black -l 120 --check prtg_pyprobe
    - black -l 120 --check .

check_code_style:
  stage: check
  image: $PYTHON_DEBIAN_IMAGE
  script:
    - flake8 . --benchmark

unit:
  stage: unit-test
  image: $PYTHON_DEBIAN_IMAGE
  script:
    - cd tests
    - pytest --cov prtg_pyprobe .

pages:
  stage: make-docs
  image: $PYTHON_DEBIAN_IMAGE
  only:
    - release
  script:
    - mkdocs build --strict --verbose
    - ls -al public
  artifacts:
    paths:
      - public

build-pypi-package:
  stage: pip-build
  only:
    - release
  image: $PYTHON_DEBIAN_IMAGE
  dependencies:
    - version
  before_script: []
  script:
    - python3 -m pip install --upgrade setuptools wheel twine
    - echo __version__ = \"$PYPROBE_VERSION\" > prtg_pyprobe/__init__.py
    - python3 setup.py sdist bdist_wheel
  artifacts:
    paths:
      - '$CI_PROJECT_DIR/dist/*'

deploy-pypi-pacakge:
  stage: pip-deploy
  only:
    - release
  image: $PYTHON_DEBIAN_IMAGE
  dependencies:
    - build-pypi-package
  before_script: []
  script:
    - cp $pypirc /root/.pypirc
    - python3 -m pip install --upgrade setuptools wheel twine
    - python3 -m twine upload dist/*

build-container-image:
  stage: build
  only:
    - release
  image:
    name: $KANIKO_IMAGE
    entrypoint: [ "" ]
  dependencies:
    - version
  before_script: []
  script:
    - printf '{"auths":{"%s":{"username":"%s","password":"%s"}}}' $CI_REGISTRY $CI_REGISTRY_USER $CI_REGISTRY_PASSWORD > /kaniko/.docker/config.json
    - /kaniko/executor $BUILD_ARGS --build-arg registry_path=$CI_REGISTRY_IMAGE --context $CI_PROJECT_DIR --dockerfile Dockerfile --target release --destination $CI_REGISTRY_IMAGE/pyprobe:$PYPROBE_VERSION

pack-helmchart:
  stage: build
  only:
    - release
  dependencies:
    - version
  image: $PYTHON_DEBIAN_IMAGE
  before_script: []
  script:
    - curl https://baltocdn.com/helm/signing.asc | apt-key add -
    - apt-get install apt-transport-https --yes
    - echo "deb https://baltocdn.com/helm/stable/debian/ all main" | tee /etc/apt/sources.list.d/helm-stable-debian.list
    - apt-get update
    - apt-get install helm
    - cd deployment/helm
    - helm lint pyprobe
    - helm package --app-version $PYPROBE_VERSION --version $PYPROBE_VERSION pyprobe
    - helm repo index . --url https://prtg-pyprobe.s3-eu-west-1.amazonaws.com/helm-charts/
  artifacts:
    paths:
      - $CI_PROJECT_DIR/deployment/helm/*.tgz
      - $CI_PROJECT_DIR/deployment/helm/index.yaml

create-stack-template:
  stage: build
  only:
    - release
  image: $PYTHON_DEBIAN_IMAGE
  before_script: []
  script:
    - pip3 install -r deployment/aws/requirements.txt
    - python deployment/aws/render_cloudformation_template.py
  artifacts:
    name: 'pyprobe cloudformation stack template'
    paths:
      - 'pyprobe_stack_template.json'

upload-helm-chart:
  stage: deploy
  only:
    - release
  image:
    name: amazon/aws-cli
    entrypoint: [""]
  dependencies:
    - pack-helmchart
  before_script: []
  script:
    - export AWS_SECRET_ACCESS_KEY=$AWS_ACCESS_KEY
    - export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
    - export AWS_DEFAULT_REGION=eu-west-1
    - aws s3 cp $CI_PROJECT_DIR/deployment/helm/*.tgz s3://prtg-pyprobe/helm-charts/
    - aws s3 cp $CI_PROJECT_DIR/deployment/helm/index.yaml s3://prtg-pyprobe/helm-charts/


.build-rasppi-image:
  # not used currently, we will provide documentation to self build the image
  stage: build
  only:
    - release
  image: $DEBIAN_IMAGE
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  before_script: []
  script:
    - apt-get update
    - apt-get install -y coreutils quilt parted qemu-user-static debootstrap zerofree zip dosfstools bsdtar libcap2-bin grep rsync xz-utils file git curl bc xxd kmod
    - rm pi-gen/stage2/EXPORT_NOOBS
    - touch pi-gen/stage3/SKIP
    - touch pi-gen/stage4/SKIP
    - touch pi-gen/stage4/SKIP_IMAGES
    - touch pi-gen/stage5/SKIP
    - touch pi-gen/stage5/SKIP_IMAGES
    - cp -r deployment/pi-gen/04-pyprobe pi-gen/stage2
    - cp deployment/pi-gen/config pi-gen/
    - chmod +x pi-gen/stage2/04-pyprobe/01-run.sh
    - chmod +x pi-gen/build.sh
    - cd pi-gen
    - $CI_PROJECT_DIR/pi-gen/build.sh
  artifacts:
    paths:
      - $CI_PROJECT_DIR/pi-gen/deploy/


create_gitlab_release:
  stage: deploy
  only:
    - release
  image: $DEBIAN_IMAGE
  dependencies:
    - version
  before_script: []
  script:
    - apt-get update && apt-get -y install curl
    - >
      curl
      --header "Content-Type: application/json"
      --header "PRIVATE-TOKEN: $RELEASE_TOKEN"
      --data "{ \"name\": \"$PYPROBE_VERSION\", \"tag_name\": \"$PYPROBE_VERSION\", \"ref\": \"$CI_COMMIT_REF_NAME\", \"description\": \"Release of pyprobe $PYPROBE_VERSION\", \"assets\": { \"links\": [ { \"name\": \"PyPi\", \"url\": \"https://pypi.org/project/prtg-pyprobe/\"}, {\"name\": \"Docker Registry\", \"url\": \"$CI_PROJECT_URL/container_registry\"}, { \"name\": \"Home Assistant Plugin\", \"url\": \"https://gitlab.com/paessler-labs/prtg-pyprobe-ha-plugins\"}, {\"name\": \"AWS Cloudformation Stack Template\", \"url\": \"https://gitlab.com/paessler-labs/prtg-pyprobe/-/jobs/artifacts/release/download?job=create-stack-template\"}, {\"name\": \"Helm Chart\", \"url\": \"https://artifacthub.io/packages/helm/prtg-pyprobe/pyprobe\"}] } }"
      --request POST $CI_API_V4_URL/projects/$CI_PROJECT_ID/releases

