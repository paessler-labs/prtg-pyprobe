import distutils.sysconfig
import os
from io import TextIOWrapper, BytesIO

import PyInquirer
import daemon.daemon as d
import psutil
from click.testing import CliRunner

import prtg_pyprobe.run
from prtg_pyprobe.utils import config
from prtg_pyprobe.utils.cli import (
    hash_access_key,
    check_systemd,
    sstop,
    sstart,
    sstatus,
    sinstall,
    pyprobe,
    service,
    daemon,
    dstart,
    dstop,
    dstatus,
    configure,
)


def side_effect(filename=None):
    if filename == "/etc/systemd/system/pyprobe.service":
        return TextIOWrapper(BytesIO())


def daemon_context(*args, **kwargs):
    return mock_main()


def mock_main():
    return True


def mock_iter():
    proc = MockProcess()
    return [proc]


class MockProcess(object):
    @staticmethod
    def cmdline():
        return ["bin/pyprobe", "python", "run"]

    @staticmethod
    def terminate():
        return True

    @staticmethod
    def name():
        return "pyprobe"

    @property
    def pid(self):
        return 1234

    @staticmethod
    def create_time():
        return 1600929804.65

    @staticmethod
    def username():
        return "root"


def raise_keyboard_interrupt(*args, **kwargs):
    raise KeyboardInterrupt


def return_true(*args, **kwargs):
    return True


def return_false(*args, **kwargs):
    return False


def return_answers(*args, **kwargs):
    return {"continue": "yes", "probe_access_key": "1234"}


def return_mock_config(*args, **kwargs):
    return MockConfig()


class MockConfig:
    def __init__(self, *args, **kwargs):
        pass

    @staticmethod
    def write(self, *args, **kwargs):
        return True


def test_hash_access_key():
    assert hash_access_key("1234") == "7110eda4d09e062aa5e4a390b0a572ac0d2c0220"
    assert not hash_access_key("1234") == "1234"


def test_check_systemd_true(fake_process):
    fake_process.register_subprocess(["ps", "--no-headers", "-o", "comm", "1"], stdout=[b"systemd\n"])
    assert check_systemd()


def test_check_systemd_false(fake_process):
    fake_process.register_subprocess(["ps", "--no-headers", "-o", "comm", "1"], stdout=[b"init\n"])
    assert not check_systemd()


class TestCLI:
    def test_cli_stop_systemd(self, monkeypatch, fake_process):
        runner = CliRunner()
        fake_process.register_subprocess(["ps", "--no-headers", "-o", "comm", "1"], stdout=[b"systemd\n"])
        fake_process.register_subprocess(["service", "pyprobe", "stop"], returncode=0)
        response = runner.invoke(sstop)
        assert "Service stopped" in response.output

    def test_cli_stop_no_systemd(self, fake_process):
        runner = CliRunner()
        fake_process.register_subprocess(["ps", "--no-headers", "-o", "comm", "1"], stdout=[b"init\n"])
        fake_process.register_subprocess(["service", "pyprobe", "stop"], returncode=0)
        response = runner.invoke(sstop)
        assert "Service only supported on systems running systemd" in response.output

    def test_cli_start_systemd(self, fake_process):
        runner = CliRunner()
        fake_process.register_subprocess(["ps", "--no-headers", "-o", "comm", "1"], stdout=[b"systemd\n"])
        fake_process.register_subprocess(["service", "pyprobe", "start"], returncode=0)
        response = runner.invoke(sstart)
        assert "Service started" in response.output

    def test_cli_start_no_systemd(self, fake_process):
        runner = CliRunner()
        fake_process.register_subprocess(["ps", "--no-headers", "-o", "comm", "1"], stdout=[b"init\n"])
        response = runner.invoke(sstart)
        assert "Service only supported on systems running systemd" in response.output

    def test_cli_status_systemd(self, fake_process):
        runner = CliRunner()
        fake_process.register_subprocess(["ps", "--no-headers", "-o", "comm", "1"], stdout=[b"systemd\n"])
        fake_process.register_subprocess(["service", "pyprobe", "status"], returncode=0)
        response = runner.invoke(sstatus)
        assert response.exit_code == 0

    def test_cli_status_no_systemd(self, fake_process):
        runner = CliRunner()
        fake_process.register_subprocess(["ps", "--no-headers", "-o", "comm", "1"], stdout=[b"init\n"])
        response = runner.invoke(sstatus)
        assert "Service only supported on systems running systemd" in response.output

    def test_cli_install_no_systemd(self, fake_process):
        runner = CliRunner()
        fake_process.register_subprocess(["ps", "--no-headers", "-o", "comm", "1"], stdout=[b"init\n"])
        response = runner.invoke(sinstall)
        assert "Service only supported on systems running systemd" in response.output

    def test_cli_install_systemd(self, monkeypatch, fake_process, tmpdir, mocker):
        runner = CliRunner()
        fake_process.register_subprocess(["ps", "--no-headers", "-o", "comm", "1"], stdout=[b"systemd\n"])
        fake_process.register_subprocess(["systemctl", "daemon-reload"], returncode=0)
        fake_process.register_subprocess(["systemctl", "enable", "pyprobe.service"], returncode=0)
        monkeypatch.setattr(distutils.sysconfig, "get_python_lib", "../")
        m = mocker.mock_open()
        m.side_effect = side_effect()
        mocker.patch("builtins.open", m)
        response = runner.invoke(sinstall)
        assert "Service install done" in response.output

    def test_cli_configure_abort(self, monkeypatch):
        runner = CliRunner()
        monkeypatch.setattr(os, "getenv", raise_keyboard_interrupt)
        response = runner.invoke(configure)
        assert "Exiting configuration wizard" in response.output

    def test_cli_configure(self, monkeypatch):
        runner = CliRunner()
        monkeypatch.setattr(os, "getenv", return_false)
        monkeypatch.setattr(os.path, "exists", return_false)
        monkeypatch.setattr(os, "mkdir", return_true)
        monkeypatch.setattr(os.path, "isfile", return_true)
        monkeypatch.setattr(PyInquirer, "prompt", return_answers)
        monkeypatch.setattr(config, "ProbeConfig", return_mock_config)
        response = runner.invoke(configure)
        assert "Completing this config wizard will overwrite your current config!" in response.output
        assert "Config written to /etc/pyprobe/config.yml" in response.output

    def test_pyprobe_group(self):
        runner = CliRunner()
        response = runner.invoke(pyprobe)
        assert "Usage: pyprobe [OPTIONS] COMMAND [ARGS]..." in response.output

    def test_service_group(self):
        runner = CliRunner()
        response = runner.invoke(service)
        assert "Usage: service [OPTIONS] COMMAND [ARGS]..." in response.output

    def test_daemon_group(self):
        runner = CliRunner()
        response = runner.invoke(daemon)
        assert "Usage: daemon [OPTIONS] COMMAND [ARGS]..." in response.output

    def test_cli_dstart(self, monkeypatch):
        runner = CliRunner()
        monkeypatch.setattr(distutils.sysconfig, "get_python_lib", "../")
        monkeypatch.setattr(d, "DaemonContext", daemon_context)
        monkeypatch.setattr(prtg_pyprobe.run, "main", mock_main)
        response = runner.invoke(dstart)
        assert "pyprobe daemon starting" in response.output

    def test_cli_dstop(self, monkeypatch):
        runner = CliRunner()
        monkeypatch.setattr(psutil, "process_iter", mock_iter)
        monkeypatch.setattr(psutil.Process, "terminate", mock_main)
        response = runner.invoke(dstop)
        assert "pyprobe daemon terminated" in response.output

    def test_cli_dstatus(self, monkeypatch):
        runner = CliRunner()
        monkeypatch.setattr(psutil, "process_iter", mock_iter)
        response = runner.invoke(dstatus)
        assert "--- Status pyprobe ---\nPID: 1234\nRunning as: root\nRunning since: 2020-09-24" in response.output
