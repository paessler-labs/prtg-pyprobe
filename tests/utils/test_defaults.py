import importlib

from prtg_pyprobe.utils import defaults as default_settings


class TestDefaults:
    def test_defaults_prod(self):
        assert default_settings.LOG_PATH == "/var/log/pyprobe/"
        assert default_settings.CONFIG_PATH == "/etc/pyprobe/"

    def test_defaults_dev(self, monkeypatch):
        monkeypatch.setenv(name="DEV", value="True")
        importlib.reload(default_settings)
        assert default_settings.LOG_PATH == ""
        assert default_settings.CONFIG_PATH == ""
