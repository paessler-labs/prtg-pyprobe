import asyncio
import socket

import pytest

from prtg_pyprobe.sensors import sensor_port as module
from prtg_pyprobe.sensors.sensor import SensorBase
from tests.conftest import asyncio_open_connection


def task_data():
    return {
        "sensorid": "1234",
        "timeout": "5",
        "host": "127.0.0.1",
        "targetport": "443",
    }


@pytest.mark.asyncio
class TestSensorPort(object):
    def test_sensor_port_base_class(self, port_sensor):
        assert type(port_sensor).__bases__[0] is SensorBase

    def test_sensor_port_name(self, port_sensor):
        assert port_sensor.name == "Port"

    def test_sensor_port_kind(self, port_sensor):
        assert port_sensor.kind == "mpport"

    def test_sensor_port_definition(self, port_sensor):
        assert port_sensor.definition == {
            "description": "Monitors the availability of a port on a target system",
            "groups": [
                {
                    "caption": "Port specific",
                    "fields": [
                        {
                            "caption": "Timeout (in s)",
                            "default": 60,
                            "help": "If the reply takes longer than this value "
                            "the request is aborted and an error message "
                            "is triggered. Maximum value is 900 sec. (= "
                            "15.0 minutes)",
                            "maximum": 900,
                            "minimum": 1,
                            "name": "timeout",
                            "required": "1",
                            "type": "integer",
                        },
                        {
                            "caption": "Port",
                            "default": 110,
                            "help": "This is the port that you want to monitor if " "it's open or closed",
                            "maximum": 65534,
                            "minimum": 1,
                            "name": "targetport",
                            "required": "1",
                            "type": "integer",
                        },
                    ],
                    "name": "portspecific",
                }
            ],
            "help": "Monitors the availability of a port on a target system",
            "kind": "mpport",
            "name": "Port",
            "tag": "mpportsensor",
        }

    @pytest.mark.asyncio
    async def test_sensor_port_work_success(self, port_sensor, monkeypatch):
        monkeypatch.setattr(asyncio, "open_connection", asyncio_open_connection)
        port_q = asyncio.Queue()
        await port_sensor.work(task_data=task_data(), q=port_q)
        out = await port_q.get()
        assert out["sensorid"] == "1234"
        assert out["message"] == "OK Port 443 available"
        assert len(out["channel"]) == 1

    @pytest.mark.asyncio
    async def test_sensor_port_work_connection_error(self, port_sensor, monkeypatch, mocker, sensor_exception_message):
        test_cases = [
            {
                "err": self.port_tcp_connection_error,
                "msg": "Port check failed. See log for details",
                "exception_text": "There has been a connection error - host:127.0.0.1 - port:443",  # see self.task_data
            },
            {
                "err": self.port_tcp_socket_gaierror,
                "msg": "Port check failed. See log for details",
                "exception_text": "There has been a connection error - host:127.0.0.1 - port:443",  # see self.task_data
            },
            {
                "err": self.port_asyncio_timeout_error,
                "msg": "Port check failed. See log for details",
                "exception_text": "There has been a connection error - host:127.0.0.1 - port:443",  # see self.task_data
            },
            {
                "err": self.port_timeout_error,
                "msg": "Port check failed. See log for details",
                "exception_text": "There has been a connection error - host:127.0.0.1 - port:443",  # see self.task_data
            },
        ]

        for tc in test_cases:
            logging_exception_mock = mocker.patch.object(module, "logging")
            monkeypatch.setattr(asyncio, "open_connection", tc["err"])
            port_q = asyncio.Queue()
            await port_sensor.work(task_data=task_data(), q=port_q)
            out = await port_q.get()
            sensor_exception_message["message"] = tc["msg"]
            assert out == sensor_exception_message
            logging_exception_mock.exception.assert_called_with(tc["exception_text"])  # see task_data()
            logging_exception_mock.exception.assert_called_once()

    async def port_tcp_success(self, *args, **kwargs):
        reader = self.RetObj()
        writer = self.RetObj()
        return reader, writer

    async def port_tcp_connection_error(self, *args, **kwargs):
        raise ConnectionError

    async def port_tcp_socket_gaierror(self, *args, **kwargs):
        raise socket.gaierror

    async def port_timeout_error(self, *args, **kwargs):
        raise TimeoutError

    async def port_asyncio_timeout_error(self, *args, **kwargs):
        raise asyncio.TimeoutError

    class RetObj(object):
        def close(self):
            return

        async def wait_closed(self):
            return
