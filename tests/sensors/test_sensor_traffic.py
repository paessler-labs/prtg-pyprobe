import asyncio

import psutil
import pytest
from psutil._common import snetio

from prtg_pyprobe.sensors import sensor_traffic as module
from prtg_pyprobe.sensors.sensor import SensorBase


def task_data_snmp_traffic(iface="en0"):
    return {
        "sensorid": "1234",
        "iface_name": iface,
        "host": "127.0.0.1",
        "kind": "mptraffic",
    }


def traffic_data(*args, **kwargs):
    return {
        "lo0": snetio(
            bytes_sent=35258368,
            bytes_recv=35258368,
            packets_sent=45529,
            packets_recv=45529,
            errin=0,
            errout=0,
            dropin=0,
            dropout=0,
        ),
        "gif0": snetio(
            bytes_sent=0,
            bytes_recv=0,
            packets_sent=0,
            packets_recv=0,
            errin=0,
            errout=0,
            dropin=0,
            dropout=0,
        ),
        "stf0": snetio(
            bytes_sent=0,
            bytes_recv=0,
            packets_sent=0,
            packets_recv=0,
            errin=0,
            errout=0,
            dropin=0,
            dropout=0,
        ),
        "en5": snetio(
            bytes_sent=1113088,
            bytes_recv=557056,
            packets_sent=4278,
            packets_recv=4459,
            errin=0,
            errout=22,
            dropin=0,
            dropout=0,
        ),
        "ap1": snetio(
            bytes_sent=0,
            bytes_recv=0,
            packets_sent=0,
            packets_recv=0,
            errin=0,
            errout=0,
            dropin=0,
            dropout=0,
        ),
        "en0": snetio(
            bytes_sent=90110976,
            bytes_recv=247672832,
            packets_sent=482754,
            packets_recv=307210,
            errin=0,
            errout=509,
            dropin=0,
            dropout=0,
        ),
        "en1": snetio(
            bytes_sent=0,
            bytes_recv=0,
            packets_sent=0,
            packets_recv=0,
            errin=0,
            errout=0,
            dropin=0,
            dropout=0,
        ),
        "en2": snetio(
            bytes_sent=0,
            bytes_recv=0,
            packets_sent=0,
            packets_recv=0,
            errin=0,
            errout=0,
            dropin=0,
            dropout=0,
        ),
        "en3": snetio(
            bytes_sent=0,
            bytes_recv=0,
            packets_sent=0,
            packets_recv=0,
            errin=0,
            errout=0,
            dropin=0,
            dropout=0,
        ),
        "en4": snetio(
            bytes_sent=0,
            bytes_recv=0,
            packets_sent=0,
            packets_recv=0,
            errin=0,
            errout=0,
            dropin=0,
            dropout=0,
        ),
        "bridge0": snetio(
            bytes_sent=0,
            bytes_recv=0,
            packets_sent=0,
            packets_recv=0,
            errin=0,
            errout=0,
            dropin=0,
            dropout=0,
        ),
        "p2p0": snetio(
            bytes_sent=0,
            bytes_recv=0,
            packets_sent=0,
            packets_recv=0,
            errin=0,
            errout=0,
            dropin=0,
            dropout=0,
        ),
        "awdl0": snetio(
            bytes_sent=0,
            bytes_recv=0,
            packets_sent=5,
            packets_recv=0,
            errin=0,
            errout=0,
            dropin=0,
            dropout=0,
        ),
        "llw0": snetio(
            bytes_sent=0,
            bytes_recv=0,
            packets_sent=0,
            packets_recv=0,
            errin=0,
            errout=0,
            dropin=0,
            dropout=0,
        ),
        "utun0": snetio(
            bytes_sent=0,
            bytes_recv=0,
            packets_sent=2,
            packets_recv=0,
            errin=0,
            errout=0,
            dropin=0,
            dropout=0,
        ),
        "utun1": snetio(
            bytes_sent=0,
            bytes_recv=0,
            packets_sent=2,
            packets_recv=0,
            errin=0,
            errout=0,
            dropin=0,
            dropout=0,
        ),
        "ppp0": snetio(
            bytes_sent=2658304,
            bytes_recv=8903680,
            packets_sent=15045,
            packets_recv=13939,
            errin=0,
            errout=0,
            dropin=0,
            dropout=0,
        ),
    }


class TestSNMPTrafficSensorProperties:
    def test_sensor_traffic_base_class(self, traffic_sensor):
        assert type(traffic_sensor).__bases__[0] is SensorBase

    def test_sensor_traffic_name(self, traffic_sensor):
        assert traffic_sensor.name == "Traffic (Probe)"

    def test_sensor_traffic_kind(self, traffic_sensor):
        assert traffic_sensor.kind == "mptraffic"

    def test_sensor_traffic_definition(self, traffic_sensor):
        assert traffic_sensor.definition == {
            "description": "Monitors traffic on the host the probe is running on.",
            "groups": [
                {
                    "caption": "Sensor Specific",
                    "fields": [
                        {
                            "caption": "Interface Name",
                            "help": "Provide the interface name to monitor.",
                            "name": "iface_name",
                            "required": "1",
                            "type": "edit",
                        }
                    ],
                    "name": "sensor_specific",
                }
            ],
            "help": "Monitors traffic on the host the probe is running on. This is always "
            "the case even if added to a different device than the Probe Device.",
            "kind": "mptraffic",
            "name": "Traffic (Probe)",
            "tag": "mptrafficsensor",
        }


@pytest.mark.asyncio
class TestSNMPTrafficSensorWork:
    async def test_sensor_traffic_work_success(self, traffic_sensor, monkeypatch):
        monkeypatch.setattr(psutil, "net_io_counters", traffic_data)
        traffic_queue = asyncio.Queue()
        await traffic_sensor.work(task_data=task_data_snmp_traffic(), q=traffic_queue)
        out = await traffic_queue.get()
        assert out["sensorid"] == "1234"
        assert out["message"] == "Interface en0"
        assert out["channel"] == [
            {
                "name": "Traffic out",
                "mode": "counter",
                "value": 90110976,
                "kind": "BytesBandwidth",
            },
            {
                "name": "Traffic in",
                "mode": "counter",
                "value": 247672832,
                "kind": "BytesBandwidth",
            },
            {
                "name": "Errors in",
                "mode": "integer",
                "value": 0,
                "kind": "Custom",
                "customunit": "",
            },
            {
                "name": "Errors out",
                "mode": "integer",
                "value": 509,
                "kind": "Custom",
                "customunit": "",
            },
            {
                "name": "Dropped in",
                "mode": "integer",
                "value": 0,
                "kind": "Custom",
                "customunit": "",
            },
            {
                "name": "Dropped out",
                "mode": "integer",
                "value": 0,
                "kind": "Custom",
                "customunit": "",
            },
        ]

    async def test_sensor_traffic_work_key_error(self, traffic_sensor, monkeypatch, mocker, sensor_exception_message):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(psutil, "net_io_counters", traffic_data)
        traffic_queue = asyncio.Queue()
        await traffic_sensor.work(task_data=task_data_snmp_traffic(iface="not_present"), q=traffic_queue)
        out = await traffic_queue.get()
        sensor_exception_message["message"] = "Interface not_present not found"
        assert out == sensor_exception_message
        logging_mock.exception.assert_called_with("Interface not found.")
        logging_mock.exception.assert_called_once()
