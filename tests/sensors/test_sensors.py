import asyncio

import psutil
import pytest
from pysnmp.error import PySnmpError
from pysnmp.hlapi import asyncio as pysnmp_async
from pysnmp.hlapi.asyncio import CommunityData, OctetString

from prtg_pyprobe.sensors import sensor as module
from prtg_pyprobe.sensors.sensor import SensorBase, SensorSNMPBase, SensorPSUtilBase


def is_end_of_mib_mock(*args, **kwargs):
    return True


async def snmp_get_success(*args, **kwargs):
    return 0, 0, 0, [("1.3.6.1.1.2.3.4", 1234), ("1.3.6.1.1.2.3.5", 5678)]


async def get_cmd_error_status(*args, **kwargs):
    return 0, OctetString("error"), 0, 0


async def get_cmd_error_indication(*args, **kwargs):
    return "error", 0, 0, 0


def cpu_load():
    return 0.1, 0.2, 0.3


class TestSensorBase:
    def test_sensor_base_attrs(self):
        assert hasattr(SensorBase, "name")
        assert hasattr(SensorBase, "kind")
        assert hasattr(SensorBase, "definition")
        assert hasattr(SensorBase, "work")

    @pytest.mark.asyncio
    async def test_sensor_base_is_abstract(self):
        abstract_sensor = SensorBase()
        with pytest.raises(NotImplementedError):
            assert abstract_sensor.name
        with pytest.raises(NotImplementedError):
            assert abstract_sensor.kind
        with pytest.raises(NotImplementedError):
            assert abstract_sensor.definition
        with pytest.raises(NotImplementedError):
            assert abstract_sensor.__getitem__("test")
        with pytest.raises(NotImplementedError):
            await abstract_sensor.work({}, asyncio.Queue())

    def test_sensor_objects_attrs(self, list_sensor_objects):
        for sensor in list_sensor_objects:
            assert type(sensor.name) is str
            assert type(sensor.kind) is str
            assert type(sensor.definition) is dict
            assert hasattr(sensor, "work")


class TestSensorPSUtilBase:
    def test_get_system_temperatures(self, mocker, psutil_system_temperatures):
        psutil_mock = mocker.patch.object(module, "psutil")
        psutil_mock.sensors_temperatures.return_value = psutil_system_temperatures

        result = SensorPSUtilBase.get_system_temperatures(fahrenheit=False)

        assert result == psutil_system_temperatures

    def test_temp_method_doesnt_exist(self, monkeypatch, psutil_system_temperatures):
        monkeypatch.delattr(psutil, "sensors_temperatures", raising=False)

        result = SensorPSUtilBase.get_system_temperatures(fahrenheit=False)

        assert not result

    def test_temp_method_returns_empty_dict(self, mocker, psutil_system_temperatures):
        psutil_mock = mocker.patch.object(module, "psutil")
        psutil_mock.sensors_temperatures.return_value = {}

        result = SensorPSUtilBase.get_system_temperatures(fahrenheit=False)

        assert not result

    def test_get_cpu_load(self, monkeypatch):
        monkeypatch.setattr(psutil, "getloadavg", cpu_load)
        assert (0.1, 0.2, 0.3) == SensorPSUtilBase.get_cpu_load()


@pytest.mark.asyncio
class TestSensorSNMPBase:
    def test_sensor_snmp_base_attrs(self):
        assert hasattr(SensorSNMPBase, "get")
        assert hasattr(SensorSNMPBase, "get_bulk")
        assert hasattr(SensorSNMPBase, "cast")
        assert hasattr(SensorSNMPBase, "construct_object_types")

    async def test_sensor_snmp_base_get_snmp_error_status(self, monkeypatch, mocker):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(pysnmp_async, "getCmd", get_cmd_error_status)
        snmp_base = SensorSNMPBase()
        cred = CommunityData("public")
        with pytest.raises(PySnmpError):
            await snmp_base.get("127.0.0.1", ["1.3.6.1.1"], cred)
        logging_mock.error.assert_called_with("error at ?")
        logging_mock.error.assert_called_once()

    async def test_sensor_snmp_base_get_snmp_success(self, snmp_custom_sensor_taskdata, monkeypatch):
        snmp_base = SensorSNMPBase()
        monkeypatch.setattr(pysnmp_async, "getCmd", snmp_get_success)
        cred = CommunityData("public")
        oid = "1.3.6.1.1.2.3.4"
        var_binds = await snmp_base.get(target="127.0.0.1", oids=[oid], credentials=cred, port=161)
        assert var_binds == [("1.3.6.1.1.2.3.4", 1234), ("1.3.6.1.1.2.3.5", 5678)]

    async def test_sensor_snmp_base_bulk_get_success(self, monkeypatch):
        snmp_base = SensorSNMPBase()
        monkeypatch.setattr(pysnmp_async, "nextCmd", snmp_get_success)
        monkeypatch.setattr(pysnmp_async, "isEndOfMib", is_end_of_mib_mock)
        cred = CommunityData("public")
        oids = ["1.3.6.1.1.2.3.4", "1.3.6.1.1.2.3.5"]
        var_binds = await snmp_base.get_bulk(target="127.0.0.1", oids=oids, credentials=cred)
        assert var_binds == [("1.3.6.1.1.2.3.4", 1234), ("1.3.6.1.1.2.3.5", 5678)]

    async def test_sensor_snmp_base_get_snmp_bulk_error_status(self, monkeypatch, mocker):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(pysnmp_async, "nextCmd", get_cmd_error_status)
        snmp_base = SensorSNMPBase()
        oids = ["1.3.6.1.1.2.3.4", "1.3.6.1.1.2.3.5"]
        cred = CommunityData("public")
        with pytest.raises(PySnmpError):
            await snmp_base.get_bulk(target="127.0.0.1", oids=oids, credentials=cred)
        logging_mock.error.assert_called_with("error at ?")
        logging_mock.error.assert_called_once()

    async def test_sensor_snmp_base_get_snmp_error_indication(self, monkeypatch, mocker):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(pysnmp_async, "nextCmd", get_cmd_error_indication)
        snmp_base = SensorSNMPBase()
        oids = ["1.3.6.1.1.2.3.4", "1.3.6.1.1.2.3.5"]
        cred = CommunityData("public")
        with pytest.raises(PySnmpError):
            await snmp_base.get_bulk(target="127.0.0.1", oids=oids, credentials=cred)
        logging_mock.error.assert_called_with("error")
        logging_mock.error.assert_called_once()

    def test_sensor_snmp_base_cast_int(
        self,
    ):
        assert SensorSNMPBase.cast("1") == 1

    def test_sensor_snmp_base_cast_float(
        self,
    ):
        assert SensorSNMPBase.cast("1.1") == 1.1

    def test_sensor_snmp_base_cast_string(
        self,
    ):
        assert SensorSNMPBase.cast("test") == "test"
